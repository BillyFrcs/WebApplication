﻿using Microsoft.EntityFrameworkCore;
using CategorySystem.Models;

namespace CategorySystem.Data                    
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<CategoryModel> Categories { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
    }
}

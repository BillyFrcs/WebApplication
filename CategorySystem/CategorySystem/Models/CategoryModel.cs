﻿using System.ComponentModel.DataAnnotations;

namespace CategorySystem.Models
{
    public class CategoryModel
    {
        [Key] public int Id { get; set; }

        [Required] public string Name { get; set; }

        public int Age { get; set; }
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;
    }
}
import Pages from "./pages/Pages";
import Category from "./components/Category";
import Search from "./components/Search";

import { BrowserRouter, Link } from "react-router-dom";
import Styled from "styled-components";
import { GiKnifeFork } from "react-icons/gi";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Nav>
          <GiKnifeFork />
          <Logo to={"/"}>Vintage Cheesy</Logo>
        </Nav>
        <Search />
        <Category />
        <Pages />
      </BrowserRouter>
    </div>
  );
}

const Logo = Styled(Link)`
     text-decoration: none;
     font-size: 1.5rem;
     font-weight: 400;
     font-family: 'Lopster Two', cursive;
`;

const Nav = Styled.div`
     padding: 4rem 0rem;
     display: flex;
     justify-content: flex-start;
     align-items: center;

     svg {
       font-size: 2rem;
     }
`;

export default App;

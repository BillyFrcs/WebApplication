import React, { useState, useEffect } from "react";

import Styled from "styled-components";
import { motion } from "framer-motion";
import { Link, useParams } from "react-router-dom";

function Cuisene() {
  const [cuisine, setCuisine] = useState([]);

  let params = useParams();

  const getCuisine = async (name) => {
    const apiKey = "ddb9ea211ffb4d35a3fb8d75d257c3b8";

    const api = await fetch(
      `https://api.spoonacular.com/recipes/complexSearch?apiKey=${apiKey}&cuisine=${name}`
    );

    const recipes = await api.json();

    setCuisine(recipes.results);
  };

  useEffect(() => {
    getCuisine(params.type);

    console.log(params.type);
  }, [params.type]);

  return (
    <Grid
      animate={{ opacity: 1 }}
      initial={{ opacity: 0 }}
      exit={{ opacity: 0 }}
      transform={{ duration: 0.5 }}
    >
      {cuisine.map((item) => {
        return (
          <Card key={item.id}>
            <Link to={"/Recipe/" + item.id}>
              <img src={item.image} alt={item.title} />
              <h4>{item.title}</h4>
            </Link>
          </Card>
        );
      })}
      ;
    </Grid>
  );
}

const Grid = Styled(motion.div)`
     display: grid;
     grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
     grid-gap: 3rem;
`;

const Card = Styled.div`
     img {
          width: 100%;
          border-radius: 2rem;
     }

     a {
          text-decoration: none;
     }

     h4 {
          text-align: center;
          padding: 1rem;
     }
`;

export default Cuisene;

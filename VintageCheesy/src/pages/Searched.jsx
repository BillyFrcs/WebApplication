import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Styled from "styled-components";
import { Link } from "react-router-dom";

function Searched() {
  const [seacrhedRecipes, setSearchedRecipes] = useState([]);

  let params = useParams();

  const getSearched = async (name) => {
    const apiKey = "ddb9ea211ffb4d35a3fb8d75d257c3b8";

    const api = await fetch(
      `https://api.spoonacular.com/recipes/complexSearch?apiKey=${apiKey}&query=${name}`
    );

    const recipes = await api.json();

    setSearchedRecipes(recipes.results);
  };

  useEffect(() => {
    getSearched(params.search);
  }, [params.search]);

  return (
    <Grid>
      {seacrhedRecipes.map((item) => {
        return (
          <Card key={item.id}>
            <Link to={"/Recipe/" + item.id}>
              <img src={item.image} alt={item.title} />
              <h4>{item.title}</h4>
            </Link>
          </Card>
        );
      })}
      ;
    </Grid>
  );
}

const Grid = Styled.div`
     display: grid;
     grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
     grid-gap: 3rem;
`;

const Card = Styled.div`
     img {
          width: 100%;
          border-radius: 2rem;
     }

     a {
          text-decoration: none;
     }

     h4 {
          text-align: center;
          padding: 1rem;
     }
`;

export default Searched;

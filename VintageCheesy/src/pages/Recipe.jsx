import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Styled from "styled-components";

function Recipe() {
  const [details, setDetails] = useState({});
  const [activeTab, setActiveTab] = useState("instructions");

  let params = useParams();

  const fetchDetails = async () => {
    const apiKey = "ddb9ea211ffb4d35a3fb8d75d257c3b8";

    const api = await fetch(
      `https://api.spoonacular.com/recipes/${params.name}/information?apiKey=${apiKey}`
    );

    const data = await api.json();

    setDetails(data);

    console.log(data);
  };

  useEffect(() => {
    fetchDetails();
  }, [params.name]);

  return (
    <Wrapper>
      <div>
        <h2>{details.title}</h2>
        <img src={details.image} alt={details.title} />
      </div>

      <Info>
        <Button
          className={activeTab === "instructions" ? "active" : ""}
          onClick={() => setActiveTab("instructions")}
        >
          Instructions
        </Button>
        <Button
          className={activeTab === "ingredients" ? "active" : ""}
          onClick={() => setActiveTab("ingredients")}
        >
          Ingredients
        </Button>
        {activeTab === "instructions" && (
          <div>
            <h3 dangerouslySetInnerHTML={{ __html: details.summary }}></h3>
            <h3 dangerouslySetInnerHTML={{ __html: details.instructions }}></h3>
          </div>
        )}

        {activeTab === "ingredients" && (
          <ul>
            {details.extendedIngredients.map((ingredient) => (
              <li key={ingredient.id}>{ingredient.original}</li>
            ))}
          </ul>
        )}
      </Info>
    </Wrapper>
  );
}

const Wrapper = Styled.div`
     margin-top: 10rem;
     margin-bottom: 5rem;
     display: flex;

     .active {
          background: linear-gradient(35deg, #494949, #313131);
          color: #ffff;
     }

     h2 {
          margin-bottom: 2rem;
     }

     li {
          font-size: 1.2rem;
          line-height: 2.5rem;
     }

     ul {
          margin-top: 2rem;
     }
`;

const Button = Styled.button`
     padding: 1rem 2rem;
     color: #313131;
     background: #ffff;
     border: 2px solid black;
     margin-right: 2rem;
     font-weight: 600;
`;

const Info = Styled.div`
     margin-left: 10rem;
`;

export default Recipe;

import React from 'react'

import { FaPizzaSlice, FaHamburger } from 'react-icons/fa'
import { GiNoodles, GiChopsticks } from 'react-icons/gi'
import Styled from "styled-components"
import { NavLink } from 'react-router-dom'

function Category()
{
     return (
          <List>
               <StyleLink to={'/cuisine/Italian'}>
                    <FaPizzaSlice />
                    <h4>Italian</h4>
               </StyleLink>

               <StyleLink to={'/cuisine/American'}>
                    <FaHamburger />
                    <h4>American</h4>
               </StyleLink>

               <StyleLink to={'/cuisine/Chinese'}>
                    <GiNoodles />
                    <h4>Chinese</h4>
               </StyleLink>

               <StyleLink to={'/cuisine/Korean'}>
                    <GiChopsticks />
                    <h4>Korean</h4>
               </StyleLink>
          </List>
     )
}

const List = Styled.div`
     display: flex;
     justify-content: center;
     margin: 2rem 0rem;
`;

const StyleLink = Styled(NavLink)`
     display: flex;
     flex-direction: column;
     justify-content: center;
     align-items: center;
     border-radius: 50%;  
     margin-right: 2rem; 
     text-decoration: none;
     background: linear-gradient(35deg, #494949, #313131);
     width: 6rem;
     height: 6rem;
     cursor: pointer;
     transform: scale(0.8);

     h4 {
          color: #ffff;
          font-size: 0.8rem;
     }

     svg {
          color: #ffff;
          font-size: 1.5rem;
     }

     &.active {
          background: linear-gradient(to right, #f27121, #e94057);

          svg {
               color: #ffff;
          }

          h4 {
               color: #ffff;
          }
     }
`;

export default Category
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Gamix Infinite</title>

        <link rel="shortcut icon" href="./favicon.svg" type="image/svg+xml">
        <link rel="stylesheet" href="./css/style.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link
            href="https://fonts.googleapis.com/css2?family=Oxanium:wght@600;700;800&family=Poppins:wght@400;500;600;700;800;900&display=swap"
            rel="stylesheet">
    </head>

    <body>
        <!-- Header -->
        <header class="header">

            <div class="header-top">
                <div class="container">
                    <div class="countdown-text">
                        Exclusive Black Friday ! Offer <span class="span skewBg">10</span> Days
                    </div>

                    <div class="social-wrapper">

                        <p class="social-title">Follow us on :</p>

                        <ul class="social-list">

                            <li>
                                <a href="" class="social-link">
                                    <ion-icon name="logo-facebook"></ion-icon>
                                </a>
                            </li>

                            <li>
                                <a href="" class="social-link">
                                    <ion-icon name="logo-twitter"></ion-icon>
                                </a>
                            </li>

                            <li>
                                <a href="" class="social-link">
                                    <ion-icon name="logo-pinterest"></ion-icon>
                                </a>
                            </li>

                            <li>
                                <a href="" class="social-link">
                                    <ion-icon name="logo-linkedin"></ion-icon>
                                </a>
                            </li>

                        </ul>

                    </div>

                </div>
            </div>
        </header>

        <!-- Blog -->
        <section class="section blog" id="blog" aria-label="blog">
            <div class="container">

                <h2 class="h2 section-title">
                    Latest News & <span class="span">Articles</span>
                </h2>

                <p class="section-text">
                    Compete With 100 Players On A Remote Island For Winner Takes Showdown Known Issue Where
                    Certain Skin
                    Strategic
                </p>

                <ul class="blog-list">

                    <li>
                        <div class="blog-card">

                            <figure class="card-banner img-holder" style="--width: 400; --height: 290;">
                                <img src="./images/blog-1.jpg" width="400" height="290" loading="lazy"
                                    alt="Shooter Action Video" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <ul class="card-meta-list">

                                    <li class="card-meta-item">
                                        <ion-icon name="person-outline"></ion-icon>

                                        <a href="#" class="item-text">Admin</a>
                                    </li>

                                    <li class="card-meta-item">
                                        <ion-icon name="calendar-outline"></ion-icon>

                                        <time datetime="2022-09-19" class="item-text">September 19, 2022</time>
                                    </li>

                                </ul>

                                <h3>
                                    <a href="#" class="card-title">Shooter Action Video</a>
                                </h3>

                                <p class="card-text">
                                    Compete With 100 Players On A Remote Island Thats Winner Takes Showdown
                                    Known Issue.
                                </p>

                                <a href="#" class="card-link">
                                    <span class="span">Read More</span>

                                    <ion-icon name="caret-forward"></ion-icon>
                                </a>

                            </div>

                        </div>
                    </li>

                    <li>
                        <div class="blog-card">

                            <figure class="card-banner img-holder" style="--width: 400; --height: 290;">
                                <img src="./images/blog-2.jpg" width="400" height="290" loading="lazy"
                                    alt="Wizard Candy" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <ul class="card-meta-list">

                                    <li class="card-meta-item">
                                        <ion-icon name="person-outline"></ion-icon>

                                        <a href="#" class="item-text">Admin</a>
                                    </li>

                                    <li class="card-meta-item">
                                        <ion-icon name="calendar-outline"></ion-icon>

                                        <time datetime="2022-09-19" class="item-text">September 19, 2022</time>
                                    </li>

                                </ul>

                                <h3>
                                    <a href="#" class="card-title">Wizard Candy</a>
                                </h3>

                                <p class="card-text">
                                    Compete With 100 Players On A Remote Island Thats Winner Takes Showdown
                                    Known Issue.
                                </p>

                                <a href="#" class="card-link">
                                    <span class="span">Read More</span>

                                    <ion-icon name="caret-forward"></ion-icon>
                                </a>

                            </div>

                        </div>
                    </li>

                    <li>
                        <div class="blog-card">

                            <figure class="card-banner img-holder" style="--width: 400; --height: 290;">
                                <img src="./images/blog-3.jpg" width="400" height="290" loading="lazy"
                                    alt="Defense Of The Ancients" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <ul class="card-meta-list">

                                    <li class="card-meta-item">
                                        <ion-icon name="person-outline"></ion-icon>

                                        <a href="#" class="item-text">Admin</a>
                                    </li>

                                    <li class="card-meta-item">
                                        <ion-icon name="calendar-outline"></ion-icon>

                                        <time datetime="2022-09-19" class="item-text">September 19, 2022</time>
                                    </li>

                                </ul>

                                <h3>
                                    <a href="#" class="card-title">Defense Of The Ancients</a>
                                </h3>

                                <p class="card-text">
                                    Compete With 100 Players On A Remote Island Thats Winner Takes Showdown
                                    Known Issue.
                                </p>

                                <a href="#" class="card-link">
                                    <span class="span">Read More</span>

                                    <ion-icon name="caret-forward"></ion-icon>
                                </a>

                            </div>

                        </div>
                    </li>

                </ul>

                <ul class="blog-list" style="margin-top: 50px;">

                    <li>
                        <div class="blog-card">

                            <figure class="card-banner img-holder" style="--width: 400; --height: 290;">
                                <img src="./images/fifa23.png" width="400" height="290" loading="lazy"
                                    alt="Shooter Action Video" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <ul class="card-meta-list">

                                    <li class="card-meta-item">
                                        <ion-icon name="person-outline"></ion-icon>

                                        <a href="#" class="item-text">Admin</a>
                                    </li>

                                    <li class="card-meta-item">
                                        <ion-icon name="calendar-outline"></ion-icon>

                                        <time datetime="2022-09-19" class="item-text">September 19, 2022</time>
                                    </li>

                                </ul>

                                <h3>
                                    <a href="#" class="card-title">FIFA 2023</a>
                                </h3>

                                <p class="card-text">
                                    Compete With 100 Players On A Remote Island Thats Winner Takes Showdown
                                    Known Issue.
                                </p>

                                <a href="#" class="card-link">
                                    <span class="span">Read More</span>

                                    <ion-icon name="caret-forward"></ion-icon>
                                </a>

                            </div>

                        </div>
                    </li>

                    <li>
                        <div class="blog-card">

                            <figure class="card-banner img-holder" style="--width: 400; --height: 290;">
                                <img src="./images/pubgpc.jpg" width="400" height="290" loading="lazy" alt="PUBG"
                                    class="img-cover">
                            </figure>

                            <div class="card-content">

                                <ul class="card-meta-list">

                                    <li class="card-meta-item">
                                        <ion-icon name="person-outline"></ion-icon>

                                        <a href="#" class="item-text">Admin</a>
                                    </li>

                                    <li class="card-meta-item">
                                        <ion-icon name="calendar-outline"></ion-icon>

                                        <time datetime="2022-09-19" class="item-text">September 19, 2022</time>
                                    </li>

                                </ul>

                                <h3>
                                    <a href="#" class="card-title">PUBG</a>
                                </h3>

                                <p class="card-text">
                                    Compete With 100 Players On A Remote Island Thats Winner Takes Showdown
                                    Known Issue.
                                </p>

                                <a href="#" class="card-link">
                                    <span class="span">Read More</span>

                                    <ion-icon name="caret-forward"></ion-icon>
                                </a>

                            </div>

                        </div>
                    </li>

                    <li>
                        <div class="blog-card">

                            <figure class="card-banner img-holder" style="--width: 400; --height: 290;">
                                <img src="./images/fortnite.jpg" width="400" height="290" loading="lazy"
                                    alt="Defense Of The Ancients" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <ul class="card-meta-list">

                                    <li class="card-meta-item">
                                        <ion-icon name="person-outline"></ion-icon>

                                        <a href="#" class="item-text">Admin</a>
                                    </li>

                                    <li class="card-meta-item">
                                        <ion-icon name="calendar-outline"></ion-icon>

                                        <time datetime="2022-09-19" class="item-text">September 19, 2022</time>
                                    </li>

                                </ul>

                                <h3>
                                    <a href="#" class="card-title">Fortnite</a>
                                </h3>

                                <p class="card-text">
                                    Compete With 100 Players On A Remote Island Thats Winner Takes Showdown
                                    Known Issue.
                                </p>

                                <a href="#" class="card-link">
                                    <span class="span">Read More</span>

                                    <ion-icon name="caret-forward"></ion-icon>
                                </a>

                            </div>

                        </div>
                    </li>

                </ul>
            </div>
        </section>

        <!-- News letter -->
        <section class="newsletter" aria-label="newsletter">
            <div class="container">

                <div class="newsletter-card">

                    <h2 class="h2">
                        Our <span class="span">Newsletter</span>
                    </h2>

                    <form action="" class="newsletter-form">

                        <div class="input-wrapper skewBg">
                            <input type="email" name="email_address" aria-label="email"
                                placeholder="Enter your email..." required class="email-field">

                            <ion-icon name="mail-outline"></ion-icon>
                        </div>

                        <button type="submit" class="btn newsletter-btn skewBg">
                            <span class="span">Subscribe</span>

                            <ion-icon name="paper-plane" aria-hidden="true"></ion-icon>
                        </button>

                    </form>

                </div>

            </div>
        </section>

        </article>
        </main>

        <!-- Footer -->
        <footer class="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="footer-brand">
                        <a href="#" class="logo">Gamix Infinite</a>

                        <p class="footer-text">
                            Gamix Infinite marketplace the relase etras thats sheets continig passag.
                        </p>

                        <ul class="contact-list">

                            <li class="contact-item">
                                <div class="contact-icon">
                                    <ion-icon name="location"></ion-icon>
                                </div>

                                <address class="item-text">
                                    Address : Samarinda, Indonesia
                                </address>
                            </li>

                            <li class="contact-item">
                                <div class="contact-icon">
                                    <ion-icon name="headset"></ion-icon>
                                </div>

                                <a href="tel:+6281385420875" class="item-text">Phone : +6281385420875</a>
                            </li>

                            <li class="contact-item">
                                <div class="contact-icon">
                                    <ion-icon name="mail-open"></ion-icon>
                                </div>

                                <a href="mailto:gamixinfinite@game.com" class="item-text">Email :
                                    gamixinfinite@game.com</a>
                            </li>

                        </ul>

                    </div>

                    <ul class="footer-list">

                        <li>
                            <p class="footer-list-title">Products</p>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Graphics (26)</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Backgrounds (11)</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Fonts (9)</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Music (3)</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Photography (3)</a>
                        </li>

                    </ul>

                    <ul class="footer-list">

                        <li>
                            <p class="footer-list-title">Need Help?</p>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Terms & Conditions</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Privacy Policy</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Refund Policy</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Affiliate</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Use Cases</a>
                        </li>

                    </ul>

                    <div class="footer-wrapper">

                        <div class="social-wrapper">

                            <p class="footer-list-title">Follow Us</p>

                            <ul class="social-list">

                                <li>
                                    <a href="#" class="social-link" style="background-color: #3b5998">
                                        <ion-icon name="logo-facebook"></ion-icon>
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="social-link" style="background-color: #55acee">
                                        <ion-icon name="logo-twitter"></ion-icon>
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="social-link" style="background-color: #d71e18">
                                        <ion-icon name="logo-pinterest"></ion-icon>
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="social-link" style="background-color: #1565c0">
                                        <ion-icon name="logo-linkedin"></ion-icon>
                                    </a>
                                </li>

                            </ul>

                        </div>

                        <div class="footer-newsletter">

                            <p class="footer-list-title">Newsletter Sign Up</p>

                            <form action="" class="footer-newsletter">
                                <input type="email" name="email_address" aria-label="email"
                                    placeholder="Enter your email" required class="email-field">

                                <button type="submit" class="footer-btn" aria-label="submit">
                                    <ion-icon name="rocket"></ion-icon>
                                </button>
                            </form>

                        </div>

                    </div>

                </div>
            </div>

            <div class="footer-bottom">
                <div class="container">

                    <p class="copyright">
                        &copy; 2022 Gamix Infinite. All Right Reserved<a href="#" class="copyright-link"></a>
                    </p>

                    <img src="./images/footer-bottom-img.png" width="340" height="21" loading="lazy" alt=""
                        class="footer-bottom-img">

                </div>
            </div>
        </footer>

        <button id="btn-back-to-top" class="back-top-btn" aria-label="back to top" data-back-top-btn>
            <ion-icon name="caret-up"></ion-icon>
        </button>

        <script src="./js/script.js" defer></script>
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    </body>

</html>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Gamix Infinite</title>

        <link rel="shortcut icon" href="./favicon.svg" type="image/svg+xml">
        <link rel="stylesheet" href="./css/style.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link
            href="https://fonts.googleapis.com/css2?family=Oxanium:wght@600;700;800&family=Poppins:wght@400;500;600;700;800;900&display=swap"
            rel="stylesheet">
    </head>

    <body>
        <!-- Header -->
        <header class="header">

            <div class="header-top">
                <div class="container">
                    <div class="countdown-text">
                        Exclusive Black Friday ! Offer <span class="span skewBg">10</span> Days
                    </div>

                    <div class="social-wrapper">

                        <p class="social-title">Follow us on :</p>

                        <ul class="social-list">

                            <li>
                                <a href="#" class="social-link">
                                    <ion-icon name="logo-facebook"></ion-icon>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="social-link">
                                    <ion-icon name="logo-twitter"></ion-icon>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="social-link">
                                    <ion-icon name="logo-pinterest"></ion-icon>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="social-link">
                                    <ion-icon name="logo-linkedin"></ion-icon>
                                </a>
                            </li>

                        </ul>

                    </div>

                </div>
            </div>
        </header>

        <!-- Search Box -->
        <div class="search-container" data-search-box>
            <div class="input-wrapper">
                <input type="search" name="search" aria-label="search" placeholder="Search here..."
                    class="search-field">

                <button class="search-submit" aria-label="submit search" data-search-toggler>
                    <ion-icon name="search-outline"></ion-icon>
                </button>

                <button class="search-close" aria-label="close search" data-search-toggler></button>
            </div>
        </div>

        <!-- Shop -->
        <section class="section shop" id="shop" aria-label="shop" style="background-image: url('./images/shop-bg.jpg')">
            <div class="container">

                <h2 class="h2 section-title">
                    Gaming Product <span class="span">Corner</span>
                </h2>

                <p class="section-text">
                    Compete with 100 players on a remote island for winner takes showdown known issue where
                    certain skin
                    strategic
                </p>

                <ul class="has-scrollbar">

                    <li class="scrollbar-item">
                        <div class="shop-card">

                            <figure class="card-banner img-holder" style="--width: 300; --height: 260;">
                                <img src="./images/shop-img-1.jpg" width="300" height="260" loading="lazy"
                                    alt="Women Black T-Shirt" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <a href="#" class="card-badge skewBg">t-shirt</a>

                                <h3 class="h3">
                                    <a href="#" class="card-title">Women Black T-Shirt</a>
                                </h3>

                                <div class="card-wrapper">
                                    <p class="card-price">$29.00</p>

                                    <button class="card-btn">
                                        <ion-icon name="basket"></ion-icon>
                                    </button>
                                </div>

                            </div>

                        </div>
                    </li>

                    <li class="scrollbar-item">
                        <div class="shop-card">

                            <figure class="card-banner img-holder" style="--width: 300; --height: 260;">
                                <img src="./images/shop-img-2.jpg" width="300" height="260" loading="lazy"
                                    alt="Gears 5 Xbox Controller" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <a href="#" class="card-badge skewBg">x-box</a>

                                <h3 class="h3">
                                    <a href="#" class="card-title">Gears 5 Xbox Controller</a>
                                </h3>

                                <div class="card-wrapper">
                                    <p class="card-price">$29.00</p>

                                    <button class="card-btn">
                                        <ion-icon name="basket"></ion-icon>
                                    </button>
                                </div>

                            </div>

                        </div>
                    </li>

                    <li class="scrollbar-item">
                        <div class="shop-card">

                            <figure class="card-banner img-holder" style="--width: 300; --height: 260;">
                                <img src="./images/shop-img-3.jpg" width="300" height="260" loading="lazy"
                                    alt="GeForce RTX 2070" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <a href="#" class="card-badge skewBg">Graphics</a>

                                <h3 class="h3">
                                    <a href="#" class="card-title">GeForce RTX 2070</a>
                                </h3>

                                <div class="card-wrapper">
                                    <p class="card-price">$29.00</p>

                                    <button class="card-btn">
                                        <ion-icon name="basket"></ion-icon>
                                    </button>
                                </div>

                            </div>

                        </div>
                    </li>

                    <li class="scrollbar-item">
                        <div class="shop-card">

                            <figure class="card-banner img-holder" style="--width: 300; --height: 260;">
                                <img src="./images/shop-img-4.jpg" width="300" height="260" loading="lazy"
                                    alt="Virtual Reality Smiled" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <a href="#" class="card-badge skewBg">VR-Box</a>

                                <h3 class="h3">
                                    <a href="#" class="card-title">Virtual Reality Smiled</a>
                                </h3>

                                <div class="card-wrapper">
                                    <p class="card-price">$29.00</p>

                                    <button class="card-btn">
                                        <ion-icon name="basket"></ion-icon>
                                    </button>
                                </div>

                            </div>

                        </div>
                    </li>

                </ul>

                <ul class="has-scrollbar">

                    <li class="scrollbar-item">
                        <div class="shop-card">

                            <figure class="card-banner img-holder" style="--width: 300; --height: 260;">
                                <img src="./images/shop-img-1.jpg" width="300" height="260" loading="lazy"
                                    alt="Women Black T-Shirt" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <a href="#" class="card-badge skewBg">t-shirt</a>

                                <h3 class="h3">
                                    <a href="#" class="card-title">Women Black T-Shirt</a>
                                </h3>

                                <div class="card-wrapper">
                                    <p class="card-price">$29.00</p>

                                    <button class="card-btn">
                                        <ion-icon name="basket"></ion-icon>
                                    </button>
                                </div>

                            </div>

                        </div>
                    </li>

                    <li class="scrollbar-item">
                        <div class="shop-card">

                            <figure class="card-banner img-holder" style="--width: 300; --height: 260;">
                                <img src="./images/shop-img-2.jpg" width="300" height="260" loading="lazy"
                                    alt="Gears 5 Xbox Controller" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <a href="#" class="card-badge skewBg">x-box</a>

                                <h3 class="h3">
                                    <a href="#" class="card-title">Gears 5 Xbox Controller</a>
                                </h3>

                                <div class="card-wrapper">
                                    <p class="card-price">$29.00</p>

                                    <button class="card-btn">
                                        <ion-icon name="basket"></ion-icon>
                                    </button>
                                </div>

                            </div>

                        </div>
                    </li>

                    <li class="scrollbar-item">
                        <div class="shop-card">

                            <figure class="card-banner img-holder" style="--width: 300; --height: 260;">
                                <img src="./images/shop-img-3.jpg" width="300" height="260" loading="lazy"
                                    alt="GeForce RTX 2070" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <a href="#" class="card-badge skewBg">Graphics</a>

                                <h3 class="h3">
                                    <a href="#" class="card-title">GeForce RTX 2070</a>
                                </h3>

                                <div class="card-wrapper">
                                    <p class="card-price">$29.00</p>

                                    <button class="card-btn">
                                        <ion-icon name="basket"></ion-icon>
                                    </button>
                                </div>

                            </div>

                        </div>
                    </li>

                    <li class="scrollbar-item">
                        <div class="shop-card">

                            <figure class="card-banner img-holder" style="--width: 300; --height: 260;">
                                <img src="./images/shop-img-4.jpg" width="300" height="260" loading="lazy"
                                    alt="Virtual Reality Smiled" class="img-cover">
                            </figure>

                            <div class="card-content">

                                <a href="#" class="card-badge skewBg">VR-Box</a>

                                <h3 class="h3">
                                    <a href="#" class="card-title">Virtual Reality Smiled</a>
                                </h3>

                                <div class="card-wrapper">
                                    <p class="card-price">$29.00</p>

                                    <button class="card-btn">
                                        <ion-icon name="basket"></ion-icon>
                                    </button>
                                </div>

                            </div>

                        </div>
                    </li>

                </ul>
            </div>
        </section>

        <!-- Footer -->
        <footer class="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="footer-brand">
                        <a href="#" class="logo">Gamix Infinite</a>

                        <p class="footer-text">
                            Gamix Infinite marketplace the relase etras thats sheets continig passag.
                        </p>

                        <ul class="contact-list">

                            <li class="contact-item">
                                <div class="contact-icon">
                                    <ion-icon name="location"></ion-icon>
                                </div>

                                <address class="item-text">
                                    Address : Samarinda, Indonesia
                                </address>
                            </li>

                            <li class="contact-item">
                                <div class="contact-icon">
                                    <ion-icon name="headset"></ion-icon>
                                </div>

                                <a href="tel:+6281385420875" class="item-text">Phone : +6281385420875</a>
                            </li>

                            <li class="contact-item">
                                <div class="contact-icon">
                                    <ion-icon name="mail-open"></ion-icon>
                                </div>

                                <a href="mailto:gamixinfinite@game.com" class="item-text">Email :
                                    gamixinfinite@game.com</a>
                            </li>

                        </ul>

                    </div>

                    <ul class="footer-list">

                        <li>
                            <p class="footer-list-title">Products</p>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Graphics (26)</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Backgrounds (11)</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Fonts (9)</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Music (3)</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Photography (3)</a>
                        </li>

                    </ul>

                    <ul class="footer-list">

                        <li>
                            <p class="footer-list-title">Need Help?</p>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Terms & Conditions</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Privacy Policy</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Refund Policy</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Affiliate</a>
                        </li>

                        <li>
                            <a href="#" class="footer-link">Use Cases</a>
                        </li>

                    </ul>

                    <div class="footer-wrapper">

                        <div class="social-wrapper">

                            <p class="footer-list-title">Follow Us</p>

                            <ul class="social-list">

                                <li>
                                    <a href="#" class="social-link" style="background-color: #3b5998">
                                        <ion-icon name="logo-facebook"></ion-icon>
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="social-link" style="background-color: #55acee">
                                        <ion-icon name="logo-twitter"></ion-icon>
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="social-link" style="background-color: #d71e18">
                                        <ion-icon name="logo-pinterest"></ion-icon>
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="social-link" style="background-color: #1565c0">
                                        <ion-icon name="logo-linkedin"></ion-icon>
                                    </a>
                                </li>

                            </ul>

                        </div>

                        <div class="footer-newsletter">

                            <p class="footer-list-title">Newsletter Sign Up</p>

                            <form action="" class="footer-newsletter">
                                <input type="email" name="email_address" aria-label="email"
                                    placeholder="Enter your email" required class="email-field">

                                <button type="submit" class="footer-btn" aria-label="submit">
                                    <ion-icon name="rocket"></ion-icon>
                                </button>
                            </form>

                        </div>

                    </div>

                </div>
            </div>

            <div class="footer-bottom">
                <div class="container">

                    <p class="copyright">
                        &copy; 2022 Gamix Infinite. All Right Reserved<a href="#" class="copyright-link"></a>
                    </p>

                    <img src="./images/footer-bottom-img.png" width="340" height="21" loading="lazy" alt=""
                        class="footer-bottom-img">

                </div>
            </div>
        </footer>

        <button id="btn-back-to-top" class="back-top-btn" aria-label="back to top" data-back-top-btn>
            <ion-icon name="caret-up"></ion-icon>
        </button>

        <script src="./js/script.js" defer></script>
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    </body>

</html>

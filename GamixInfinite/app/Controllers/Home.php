<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function Index()
    {
        return view('Home');
    }

    public function Shop()
    {
        return view('Shop');
    }

    public function Blog()
    {
        return view('Blog');
    }
}

<h1 align="center"> Portfolio Website </h1>

My personal portfolio website, which features some of my Github Projects and Games especially my technical skills.<br/>

This project was built using these technologies.

- React.js
- Node.js
- Express.js
- CSS3
- Vercel

## Features

**📖 Multi-Page Layout**

**🎨 Styled with React-Bootstrap and Css with easy to customize colors**

**📱 Fully Responsive**

## 🛠 Installation and Setup Instructions

1. Installation: `npm install` or `npm install --legacy-peer-deps`

2. In the project directory, run: `npm start`

3. To deploy the website run: `npm run deploy`

## TL;DR

This template website was originally created by this dude [Soumyajit4419](https://github.com/soumyajit4419/)
